# ChaseQuest Map - a Hackathon Project

This is a ChaseQuest Map project which is a sample application to show MCC on
Google Map based on filter like MCC or Zip Code

## Features

- Search for places zip code
- Filter options: All/Open/High Rating/Low Price
- Use of Google Map API is via [`@googlemap-react/core`] which is a React
  wrapper for Google Map written by myself.
