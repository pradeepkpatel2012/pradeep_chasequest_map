import {mount, route} from 'navi'

export default mount({
  '/': route({
    title: 'ChaseQuest Map',
    getView: () => import('./home'),
  }),
})
