import React, {useContext, useEffect, useState} from 'react'
import {delay, debounce, throttle} from 'lodash'
import {Marker, GoogleMapContext, InfoWindow} from '@googlemap-react/core'
import {YelpBusinessesSearchResult} from '../common'

interface MapElementsProps {
  result: YelpBusinessesSearchResult
  infoDisplay: boolean
  setInfoDisplay: (show?: boolean) => void
}

export default ({result, infoDisplay, setInfoDisplay}: MapElementsProps) => {
  const decoratedContent = (content: string) => {
    if (result.merchantLimit == undefined && result.exactMatch == undefined) {
      return `
      <div style="display: flex; flex-direction: column; align-items: center; justify-content: center;">
        <strong style="font-size: calc(10px + 0.5vh); text-align: center; padding: 0 0 5px 0;">        
            ${content}        
        </strong>
        <img src=${result.image_url} alt=${result.name} width="100px" />     
          <a href=${result.url} style="margin-top: 5px">
            ${result.googlemap}
          </a>
      </div>
    `
    } else if (result.exactMatch !== undefined && result.redeemable == false) {
      return `
      <div style="display: flex; flex-direction: column; align-items: center; justify-content: center;">
        <strong style="font-size: calc(10px + 0.5vh); text-align: center; padding: 0 0 5px 0;">        
            ${content}        
        </strong>
        <br><strong>${result.categoryName}</strong>
        Distance:   ${result.distance}<br>
        Merchant Available Limit: $${result.merchantLimit}<br>
        Transaction Limit: ${result.txnLimit}<br>
        <img src=${result.image_url} alt=${result.name} width="100px" />     
          <a href=${result.url} style="margin-top: 5px">
            ${result.googlemap}
          </a>
      </div>
    `
    } else if (result.redeemable == true && result.extraMgs == undefined) {
      return `
      <div style="display: flex; flex-direction: column; align-items: center; justify-content: center;">
        <strong style="font-size: calc(10px + 0.5vh); text-align: center; padding: 0 0 5px 0;">        
            ${content}        
        </strong>
        <br><strong>${result.categoryName}</strong>
        Distance:   ${result.distance}<br>
        Merchant Available Limit: $${result.merchantLimit}<br>
        Transaction Limit: ${result.txnLimit}<br>        
        <span style="color: green; font-weight: bold;">Points can be redeemed here...</span>
        <img src=${result.image_url} alt=${result.name} width="100px" />     
          <a href=${result.url} style="margin-top: 5px">
            ${result.googlemap}
          </a>
      </div>
    `
    } else if (result.redeemable == true && result.extraMgs !== undefined) {
      return `
      <div style="display: flex; flex-direction: column; align-items: center; justify-content: center;">
        <strong style="font-size: calc(10px + 0.5vh); text-align: center; padding: 0 0 5px 0;">        
            ${content}        
        </strong>
        <br><strong>${result.categoryName}</strong>
        Distance:   ${result.distance}<br>
        Merchant Available Limit: $${result.merchantLimit}<br>
        Transaction Limit: ${result.txnLimit}<br>        
        <span style="color: red;">${result.extraMgs}</span>
        <img src=${result.image_url} alt=${result.name} width="100px" />     
          <a href=${result.url} style="margin-top: 5px">
            ${result.googlemap}
          </a>
      </div>
    `
    } else {
      return `
      <div style="display: flex; flex-direction: column; align-items: center; justify-content: center;">
        <strong style="font-size: calc(10px + 0.5vh); text-align: center; padding: 0 0 5px 0;">        
            ${content}        
        </strong>
        <br><strong>${result.categoryName}</strong>
        Distance:   ${result.distance}<br>
        <span style="color: red; font-weight: bold;">Call to PA</span>
        <img src=${result.image_url} alt=${result.name} width="100px" />     
          <a href=${result.url} style="margin-top: 5px">
            ${result.googlemap}
          </a>
      </div>
    `
    }
  }
  const marker_icon = () => {
    if (result.exactMatch) {
      return 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
    } else {
      return 'http://maps.google.com/mapfiles/ms/icons/red-dot.png'
    }
  }

  // Set handlers
  const handleClick = () => {
    setInfoDisplay()
  }
  const handleMouseOver = (event: google.maps.MouseEvent) => {
    //setInfoDisplay(true)
  }
  const handleMouseOut = (event: google.maps.MouseEvent) => {
    //delay(() => setInfoDisplay(false), 200)
  }

  return (
    <>
      <Marker
        id={result.id}
        opts={{
          position: {
            lat: result.coordinates.latitude,
            lng: result.coordinates.longitude,
          },
          icon: marker_icon(),
        }}
        onClick={handleClick}
        onMouseOver={handleMouseOver}
        onMouseOut={handleMouseOut}
      />
      <InfoWindow
        anchorId={result.id}
        opts={{
          content: decoratedContent(result.name),
        }}
        visible={infoDisplay}
        onCloseClick={() => {
          setInfoDisplay(false)
        }}
      />
    </>
  )
}
