import React, {useContext, useEffect, useState} from 'react'
import {GoogleMapContext} from '@googlemap-react/core'
import {useDebounce} from '../hooks'
import SearchResults from './SearchResults'
import {YelpBusinessesSearchResults} from '../common'
import {initialBusinesses} from '../common/defaultData'

export default () => {
  const {state} = useContext(GoogleMapContext)
  const [keyword, setKeyword] = useState('')
  const [results, setResults] = useState<YelpBusinessesSearchResults>(
    initialBusinesses,
  )
  const [loaddata, setLoaddata] = useState<YelpBusinessesSearchResults>(
    initialBusinesses,
  )
  const [filter, setFilter] = useState('all')
  const debouncedKeyword = useDebounce(keyword, 500)
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setKeyword(event.target.value)
  }
  const handleSelectChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    setFilter(event.target.value)
  }

  //const handleChange = (event: React.FormEvent<HTMLSelectElement>) => {
  const handleChange = (event: string) => {
    //handleChange (event: React.FormEvent<HTMLSelectElement>){
    console.log(event)
    if (event === 'purchase') {
      let pur = document.getElementById('p_creditLimit')
      let tne = document.getElementById('t_creditLimit')
      if (tne != undefined) {
        tne.classList.remove('displayBlock', 'displayNone')
        tne.classList.add('displayNone')
      }
      if (pur != undefined) {
        pur.classList.remove('displayBlock', 'displayNone')
        pur.classList.add('displayBlock')
      }
    } else {
      let pur = document.getElementById('p_creditLimit')
      let tne = document.getElementById('t_creditLimit')
      if (pur != undefined) {
        pur.classList.remove('displayBlock', 'displayNone')
        pur.classList.add('displayNone')
      }
      if (tne != undefined) {
        tne.classList.remove('displayBlock', 'displayNone')
        tne.classList.add('displayBlock')
      }
    }
    //do something with selectName and selectedOption
  }

  const fetchData = async () => {
    // if (debouncedKeyword === '') {
    //   return
    // }
    if (state.places) {
      if (!state.map) {
        return
      }
      const query = {
        latitude: state.map.getCenter().lat(),
        longitude: state.map.getCenter().lng(),
        radius: 1000,
        term: debouncedKeyword,
      }
      const results = Object.assign({}, loaddata)
      //console.log(JSON.stringify(results))
      results.businesses = results.businesses.filter(resp => {
        return resp.location.zip_code.includes(query.term)
      })
      if (results.businesses) setResults(results)
    }
  }

  useEffect(() => {
    fetchData()
  }, [debouncedKeyword])

  return (
    <div className="info-panel">
      <div>
        <label className="mccfilter">Account Type:</label>
        <select
          className="search-submit-button"
          id="searchType"
          onChange={e => handleChange(e.currentTarget.value)}
        >
          <option value="purchase">Purchase************3456</option>
          <option value="TNE">T&E************9867</option>
        </select>
      </div>
      <div id="p_creditLimit" className="displayBlock">
        Available Balance: $5000
      </div>
      <div id="t_creditLimit" className="displayNone">
        Available Balance: $8000
      </div>
      <hr />
      <div className="search">
        <input
          className="search-input"
          type="text"
          name="keyword"
          placeholder="Search with Zip Code"
          autoFocus
          value={keyword}
          onChange={handleInputChange}
        />
        <div className="width">
          <label className="mccfilter">Filter By Category:</label>
          <select
            className="search-submit-button"
            onChange={handleSelectChange}
          >
            <option value="all">All</option>
            <option value="TNE">Restaurant</option>
            <option value="Hotel">Lodging</option>
          </select>
        </div>
      </div>
      <label className="mcc">Searched Results:</label>
      <ul className="search-results">
        {Array.isArray(results.businesses) && results.businesses.length > 0 ? (
          <SearchResults results={results} filter={filter} />
        ) : (
          debouncedKeyword !== '' && <li>No results found</li>
        )}
      </ul>
    </div>
  )
}
