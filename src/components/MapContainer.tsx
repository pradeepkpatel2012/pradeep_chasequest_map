import React, {useEffect, useState} from 'react'
import {MapBox, Marker, InfoWindow} from '@googlemap-react/core'
import {delay} from 'lodash'

export default () => {
  const [center, setCenter] = useState({lat: 19.166925, lng: 72.8559825})
  useEffect(() => {
    if (navigator.geolocation)
      navigator.geolocation.getCurrentPosition((position: Position) =>
        setCenter({
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        }),
      )
  }, [])

  const [show, setShow] = useState(false)
  useEffect(() => {
    setShow(show)
    console.log('show -> ' + show)
  }, [show])

  const [infoDisplay, setInfoDisplay] = useState(false)
  useEffect(() => {
    setInfoDisplay(infoDisplay)
    console.log('infoDisplay -> ' + infoDisplay)
  }, [infoDisplay])

  const decoratedContent = (content: string) => `
    <div style="display: flex; flex-direction: column; align-items: center; justify-content: center;">
      <strong style="font-size: calc(10px + 0.5vh); text-align: center; padding: 0 0 5px 0;">        
          ${content}        
      </strong>
      <a href='http://www.google.com/maps/place/${center.lat},${
    center.lng
  }' style="margin-top: 5px">
          View on Google Maps
        </a>
    </div>
  `
  // Set handlers
  const handleClick = () => {
    setShow(true)
  }
  const handleMouseOver = (event: google.maps.MouseEvent) => {
    console.log('Hover')
    //setInfoDisplay(true)
  }
  const handleMouseOut = (event: google.maps.MouseEvent) => {
    //delay(() => setInfoDisplay(false), 200)
  }

  return (
    <>
      <div className="map-container">
        <MapBox
          apiKey="AIzaSyC6I-uL4lzPx0CzyOzyYSdnibxVrsfVy6g"
          opts={{
            center: center,
            zoom: 14,
          }}
          className="map-content"
          style={{}}
          usePlaces
        />
      </div>
      <Marker
        id={'12345'}
        opts={{
          position: {
            lat: center.lat,
            lng: center.lng,
          },
        }}
        onClick={handleClick}
        onMouseOver={handleMouseOver}
        onMouseOut={handleMouseOut}
      />
      <InfoWindow
        anchorId={'12345'}
        opts={{
          content: decoratedContent('Your Current Location'),
        }}
        visible={infoDisplay || show}
        onCloseClick={() => {
          setInfoDisplay(false)
          setShow(false)
        }}
      />
    </>
  )
}
